from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from shop.admin.defaults import customer
from shop.admin.defaults.order import OrderAdmin
from shop.models.defaults.order import Order
from shop.admin.order import PrintInvoiceAdminMixin
from adminsortable2.admin import SortableAdminMixin
from shop.admin.product import UnitPriceMixin, InvalidateProductCacheMixin, SearchProductIndexMixin
from myshop.models import Manufacturer, SmartCard, Image


admin.site.site_header = "My SHOP Administration"


@admin.register(Order)
class OrderAdmin(PrintInvoiceAdminMixin, OrderAdmin):
    pass


admin.site.register(Manufacturer, admin.ModelAdmin)

__all__ = ['customer']


class ImageInline(admin.TabularInline):
    model = Image
    extra = 4

@admin.register(SmartCard)
class SmartCardAdmin(InvalidateProductCacheMixin, SearchProductIndexMixin, SortableAdminMixin, UnitPriceMixin, admin.ModelAdmin):
    fieldsets = [
        (None, {
            'fields': [
                ('product_name', 'slug'),
                ('product_code', 'unit_price'),
                'active',
            ],
        }),
        (_("Translatable Fields"), {
            'fields': ['caption', 'description'],
        }),
        (_("Properties"), {
            'fields': ['manufacturer', 'storage', 'card_type'],
        }),
    ]
    inlines = [ImageInline]
    prepopulated_fields = {'slug': ['product_name']}
    list_display = ['product_name', 'product_code', 'get_unit_price', 'active']
    search_fields = ['product_name']
