from django.conf.urls import url

from bootcamp.qa import views

app_name = 'qa'
urlpatterns = [
    url(r'^quizes/$', views.LoadQuizes.as_view(), name='quizes'),
    url(r'^quizes/(?P<slug>[\w-]+)/$', views.LoadQuiz.as_view(), name='quiz'),
    url(r'^activity/$', views.SaveActivity.as_view(), name='save_activity'),
    url(r'^vcourse/$', views.LoadVocabularyCourses.as_view(), name='vocabulary_courses'),
    url(r'^vbookmark/$', views.LoadVocabularyBookmarked.as_view(), name='vocabulary_bookmarked'),
    url(r'^vbookmark/(?P<id>\d+)/$', views.SaveVocabularyBookmark.as_view(), name='vocabulary_save_bookmark'),
    url(r'^vcourse/(?P<id>\d+)/$', views.LoadVocabularyCourse.as_view(), name='vocabulary_course'),
    url(r'^vprogress/$', views.SaveVocabularyProgress.as_view(), name='save_vocabulary_progress'),
]
