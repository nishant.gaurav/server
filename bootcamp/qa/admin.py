from django import forms
from django.contrib import admin
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.utils.translation import ugettext_lazy as _
from import_export.admin import ImportExportModelAdmin

from .resources import QuizResource, CategoryResource, QuestionResource, AnswerResource
from .models import Quiz, Category, SubCategory, Progress, Question, Answer, SlideShow, Word, VocabularyCourse


class AnswerInline(admin.TabularInline):
    model = Answer
    extra = 4

class SlideShowInline(admin.TabularInline):
    model = SlideShow
    extra = 1

class QuizAdminForm(forms.ModelForm):
    """
    below is from
    http://stackoverflow.com/questions/11657682/
    django-admin-interface-using-horizontal-filter-with-
    inline-manytomany-field
    """

    class Meta:
        model = Quiz
        exclude = []

    questions = forms.ModelMultipleChoiceField(
        queryset=Question.objects.all(),
        required=False,
        label=_("Questions"),
        widget=FilteredSelectMultiple(
            verbose_name=_("Questions"),
            is_stacked=False))

    prerequisites = forms.ModelMultipleChoiceField(
        queryset=Quiz.objects.all(),
        required=False,
        label=_("Prerequisites"),
        widget=FilteredSelectMultiple(
            verbose_name=_("Prerequisites"),
            is_stacked=False))

    def __init__(self, *args, **kwargs):
        super(QuizAdminForm, self).__init__(*args, **kwargs)
        if self.instance.pk:
            self.fields['questions'].initial =\
                self.instance.question_set.all()

    def save(self, commit=True):
        quiz = super(QuizAdminForm, self).save(commit=False)
        quiz.save()
        quiz.question_set.set(self.cleaned_data['questions'])
        self.save_m2m()
        return quiz


class QuizAdmin(ImportExportModelAdmin):
    form = QuizAdminForm
    resource_class = QuizResource

    list_display = ('title', 'category', )
    list_filter = ('category',)
    search_fields = ('description', 'category', )


class CategoryAdmin(ImportExportModelAdmin):
    search_fields = ('category', )
    resource_class = CategoryResource


class SubCategoryAdmin(admin.ModelAdmin):
    search_fields = ('sub_category', )
    list_display = ('sub_category', 'category',)
    list_filter = ('category',)


class QuestionAdmin(ImportExportModelAdmin):
    list_display = ('content', 'category', 'id', 'level')
    resource_class = QuestionResource
    list_filter = ('category','sub_category','quiz')
    fields = ('version', 'passage', 'global_variables', 'content', 'category', 'sub_category', 'tags',
              'hint', 'explanation', 'quiz', 'instructions', 'question_help',
              'level', 'question_type', 'answer_text', 'answer_order')

    search_fields = ('content', 'hint', 'explanation')
    filter_horizontal = ('quiz',)

    inlines = [AnswerInline, SlideShowInline]


class VocabularyCourseAdmin(admin.ModelAdmin):
    list_display = ('title',)

class WordAdmin(admin.ModelAdmin):
    search_fields = ('spelling', 'meaning', 'tip', 'confusing')
    list_display = ('level', 'spelling', 'parts_of_speech', 'course')
    list_filter = ('course','level')

class ProgressAdmin(admin.ModelAdmin):
    """
    to do:
            create a user section
    """
    search_fields = ('user', 'score', )

class AnswerAdmin(ImportExportModelAdmin):
    resource_class = AnswerResource

admin.site.register(Answer, AnswerAdmin)
admin.site.register(Quiz, QuizAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(SubCategory, SubCategoryAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Progress, ProgressAdmin)
admin.site.register(VocabularyCourse, VocabularyCourseAdmin)
admin.site.register(Word, WordAdmin)
