from rest_framework import serializers
from markdownx.utils import markdownify

from bootcamp.qa import models

class QuizSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Quiz
        fields = '__all__'

class QuizListSerializer(serializers.ListSerializer):
    child = QuizSerializer()
    many = True
    allow_null = True

class ActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Activity
        fields = '__all__'

class WordSerializer(serializers.ModelSerializer):
    bookmark = serializers.BooleanField(read_only=True)
    class Meta:
        model = models.Word
        fields = ('id','level','lcd','spelling','parts_of_speech','meaning','tone','tip','confusing','context','bookmark')

class WordListSerializer(serializers.ListSerializer):
    child = WordSerializer()
    many = True
    allow_null = True

class ProgressUserWordSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ProgressUserWord
        fields = '__all__'

class ProgressUserWordListSerializer(serializers.ListSerializer):
    child = ProgressUserWordSerializer()
    many = True
    allow_null = True

class VCourseSerializer(serializers.ModelSerializer):
    num_words = serializers.IntegerField(read_only=True)
    num_mastered = serializers.IntegerField(read_only=True)
    class Meta:
        model = models.VocabularyCourse
        fields = ('id', 'title', 'level', 'num_words', 'num_mastered')

class VCourseListSerializer(serializers.ListSerializer):
    child = VCourseSerializer()
    many = True
    allow_null = True

class AnswerSerializer(serializers.ModelSerializer):
    content = serializers.CharField(source='get_content', read_only=True)
    class Meta:
        model = models.Answer
        fields = ('content','correct')

class QuestionSerializer(serializers.ModelSerializer):
    passage = serializers.CharField(source='get_passage', read_only=True)
    content = serializers.CharField(source='get_content', read_only=True)
    answers = AnswerSerializer(source='answer_set', many=True, read_only=True)
    hint = serializers.CharField(source='get_hint', read_only=True)
    explanation = serializers.CharField(source='get_explanation', read_only=True)
    class Meta:
        model = models.Question
        fields = '__all__'

class QuestionListSerializer(serializers.ListSerializer):
    child = QuestionSerializer()
    many = True
    allow_null = True
