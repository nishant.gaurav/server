from import_export import resources
from .models import Quiz, Category, Question, Answer


class AnswerResource(resources.ModelResource):
    class Meta:
        model = Answer


class QuestionResource(resources.ModelResource):
    class Meta:
        model = Question


class QuizResource(resources.ModelResource):
    class Meta:
        model = Quiz


class CategoryResource(resources.ModelResource):
    class Meta:
        model = Category