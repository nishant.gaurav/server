import json
from django.shortcuts import get_object_or_404
from django.db.models import Exists, OuterRef, Count, IntegerField, Subquery, Case, When

from bootcamp.qa import models
from bootcamp.qa import serializers

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status, permissions

class LoadQuizes(APIView):

    def get(self, request):
        quizes = models.Quiz.objects.filter(draft=False)
        quizes_data = serializers.QuizListSerializer(quizes).data
        return Response(quizes_data)

class LoadQuiz(APIView):

    def get(self, request, slug):
        quiz = get_object_or_404(models.Quiz,url=slug)
        session = models.Session.objects.user_session(user=request.user, quiz=quiz)
        preserved = Case(*[When(id=id, then=pos) for pos, id in enumerate(session.questions)])
        questions = quiz.question_set.filter(id__in=session.questions).order_by(preserved)
        questions_data = serializers.QuestionListSerializer(questions).data
        return Response(questions_data)


class SQCount(Subquery):
    template = "(SELECT count(*) FROM (%(subquery)s) _count)"
    output_field = IntegerField()

class LoadVocabularyCourses(APIView):

    def get(self, request):
        progress = models.ProgressUserWord.objects.filter(word=OuterRef('pk'), user=request.user, status=2)
        words = models.Word.objects.annotate(mastered=Exists(progress)).filter(mastered=True)
        courses = models.VocabularyCourse.objects.annotate(num_words=Count('word')).annotate(num_mastered=SQCount(words.filter(course=OuterRef('pk')).values('pk')))
        courses_data = serializers.VCourseListSerializer(courses).data
        return Response(courses_data)

class LoadVocabularyCourse(APIView):

    def get(self, request, id):
        course = get_object_or_404(models.VocabularyCourse,id=id)
        has_bookmark = models.BookmarkWord.objects.filter(word=OuterRef('pk'),user=request.user)
        words = models.Word.objects.filter(course=course).annotate(bookmark=Exists(has_bookmark))
        words_data = serializers.WordListSerializer(words).data
        progress = models.ProgressUserWord.objects.filter(user=request.user, word__in=words)
        progress_data = serializers.ProgressUserWordListSerializer(progress).data
        return Response({'words':words_data,'progress':progress_data})

class LoadVocabularyBookmarked(APIView):

    def get(self, request):
        words = models.Word.objects.filter(bookmarkword__user=request.user)
        words_data = serializers.WordListSerializer(words).data
        return Response(words_data)

class SaveVocabularyBookmark(APIView):

    def get(self, request, id):
        word = get_object_or_404(models.Word,id=id)
        bm, created = models.BookmarkWord.objects.get_or_create(user=request.user, word=word)
        if not created:
            bm.delete()
        return Response(status=status.HTTP_202_ACCEPTED)

class SaveVocabularyProgress(APIView):

    def post(self, request):
        progress_list = json.loads(request.POST.get("progress_list", None))
        try:
            for progress in progress_list:
                word = get_object_or_404(models.Word, id=progress['word'])
                p, _ = models.ProgressUserWord.objects.get_or_create(user=request.user, word=word)
                p.status = progress['status']
                p.save()
            return Response(status=status.HTTP_202_ACCEPTED)
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST)

class SaveActivity(APIView):

    def post(self, request):
        session = models.Session.objects.get(id=request.data["session"])
        question = models.Question.objects.get(id=request.data["question"])
        temp = request.data
        temp["user"] = request.user.id
        temp["question_version"] = str(question.updated_at)
        temp["quiz"] = session.quiz.id
        serialized = serializers.ActivitySerializer(data=temp)
        if serialized.is_valid():
            serialized.save()
            return Response(status=status.HTTP_202_ACCEPTED)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
