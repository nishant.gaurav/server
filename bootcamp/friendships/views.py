from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.conf import settings

try:
    from django.contrib.auth import get_user_model

    user_model = get_user_model()
except ImportError:
    from django.contrib.auth.models import User

    user_model = User

from django.shortcuts import render, get_object_or_404, redirect

from .exceptions import AlreadyExistsError
from .models import Friend, Follow, FriendshipRequest, Block

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import generics, status
from django.db.models import F, Q, IntegerField, CharField, ExpressionWrapper, Value
from django.db.models.functions import Least
from django.contrib.gis.db.models.functions import Distance
from bootcamp.users.serializers import UserSerializer, UserListSerializer
from bootcamp.messager.models import MessageThread

from datetime import timedelta
from itertools import chain


class LikeApiView(APIView):

    def get(self, request):
        to_user = get_object_or_404(user_model, id=request.GET['id'])
        from_user = request.user
        if not request.user.add_like():
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
        if FriendshipRequest.objects.filter(
            from_user=to_user, to_user=from_user, viewed__isnull=True
        ).exists():
            FriendshipRequest.objects.get(
                from_user=to_user, to_user=from_user).accept()
            return Response(UserSerializer(request.user).data)#"matched"
        try:
            Friend.objects.add_friend(from_user, to_user)
        except AlreadyExistsError:
            #TODO log this error and see why it happens
            f_request = FriendshipRequest.objects.get(from_user=from_user, to_user=to_user)
            if f_request.viewed:
                f_request.rejected = f_request.viewed
                f_request.created = timezone.now()
                f_request.viewed = None
                f_request.save()
                return Response(UserSerializer(request.user).data)#"already_rejected"
            else:
                f_request.created = timezone.now()
                f_request.save()
                return Response(UserSerializer(request.user).data)#"created_updated"
        else:
            FriendshipRequest.objects.filter(from_user=to_user, to_user=from_user, viewed__isnull=False).delete()
            return Response(UserSerializer(request.user).data)#"liked"

    def post(self, request):
        to_user = get_object_or_404(user_model, id=request.POST.get("id", ""))
        from_user = request.user
        if not request.user.add_superlike():
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
        if FriendshipRequest.objects.filter(
            from_user=to_user, to_user=from_user, viewed__isnull=True
        ).exists():
            FriendshipRequest.objects.get(
                from_user=to_user, to_user=from_user).accept()
            return Response(UserSerializer(request.user).data)#"matched"
        message = request.POST.get("message", "").strip()
        try:
            if len(message) > 0 and len(message) <= 200:
                Friend.objects.add_friend(from_user, to_user, message)
            else:
                Friend.objects.add_friend(from_user, to_user)
        except AlreadyExistsError:
            #TODO log this error and see why it happens
            f_request = FriendshipRequest.objects.get(from_user=from_user, to_user=to_user)
            if len(message) >= 20 and len(message) <= 200:
                f_request.message = message
            if request.viewed:
                f_request.rejected = f_request.viewed
                f_request.created = timezone.now()
                f_request.viewed = None
                f_request.save()
                return Response(UserSerializer(request.user).data)#"already_rejected"
            else:
                f_request.created = timezone.now()
                f_request.save()
                return Response(UserSerializer(request.user).data)#"created_updated"
        else:
            FriendshipRequest.objects.filter(from_user=to_user, to_user=from_user, viewed__isnull=False).delete()
            return Response(UserSerializer(request.user).data)#"liked"

class PassApiView(APIView):

    def get(self, request):
        to_user = get_object_or_404(user_model, id=request.GET['id'])
        from_user = request.user
        try:
            Friend.objects.add_friend(to_user, from_user)
        except AlreadyExistsError:
            if FriendshipRequest.objects.filter(from_user=to_user, to_user=from_user,
                viewed__isnull=True
            ).exists():
                FriendshipRequest.objects.get(from_user=to_user, to_user=from_user).reject()
                return Response(UserSerializer(request.user).data)#"rejected_or_updated"
            else:
                FriendshipRequest.objects.get(from_user=to_user, to_user=from_user).mark_viewed()
                return Response(UserSerializer(request.user).data)#"viewed_updated"
        else:
            #This is an artificial request created for logging dislike
            #1. when artificial request gets right swipe/ left swipe, handle them
            f_request = FriendshipRequest.objects.get(from_user=to_user, to_user=from_user)
            if FriendshipRequest.objects.filter(from_user=from_user, to_user=to_user,
                viewed__isnull=False
            ).exists():
                FriendshipRequest.objects.get(from_user=from_user, to_user=to_user).delete()
                f_request.reject()
                return Response(UserSerializer(request.user).data)#"both_passed"
            else:
                #delete if any
                FriendshipRequest.objects.filter(from_user=from_user, to_user=to_user).delete()
                f_request.mark_viewed()
                return Response(UserSerializer(request.user).data)#"passed_or_updated"

class ReportApiView(APIView):
    # report fake users
    def get(self, request):
        blocked = get_object_or_404(user_model, id=request.GET['id'])
        blocker = request.user
        try:
            #TODO delete message thread, restrict video calling etc.
            Block.objects.add_block(blocker, blocked, 'P')
        except AlreadyExistsError:
            #TODO log this error and see why it happens
            return Response("already_blocked")
        else:
            return Response("blocked")

    # unmatch
    def post(self, request):
        thread = get_object_or_404(MessageThread, uuid_id=request.POST.get("thread_id", ""))
        reason = request.POST.get("reason", "N")
        if len(reason)>1:
            reason = "N"
        blocker = request.user
        for client in thread.clients.exclude(id=request.user.id):
            try:
                Block.objects.add_block(blocker, client, reason)
            except AlreadyExistsError:
                pass
        thread.is_active = False
        thread.last_activity = timezone.now()
        thread.save()
        return Response("unmatched")