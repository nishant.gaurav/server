from django.conf.urls import url
from . import views

app_name="friendships"
urlpatterns = [
    url(
        regex=r"^like/$",
        view=views.LikeApiView.as_view(),
        name="friendship_like",
    ),
    url(
        regex=r"^pass/$",
        view=views.PassApiView.as_view(),
        name="friendship_pass",
    ),
    url(
        regex=r"^report/$",
        view=views.ReportApiView.as_view(),
        name="friendship_report",
    ),
]
