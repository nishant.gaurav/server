from rest_framework import serializers

from bootcamp.messager import models
from bootcamp.users.serializers import UserSerializer


class MessageSerializer(serializers.ModelSerializer):
    sender = UserSerializer(read_only=True)
    thread_id = serializers.CharField(source='thread.uuid_id', read_only=True)
    message_id = serializers.CharField(source='id', read_only=True)

    class Meta:
        model = models.Message
        fields = ('message_id','timestamp','text','sender','thread_id')


class MessageListSerializer(serializers.ListSerializer):
    child = MessageSerializer()
    many = True
    allow_null = True


class MessageThreadSerializer(serializers.ModelSerializer):
    unread_count = serializers.IntegerField(read_only=True)
    last_message = MessageSerializer(read_only=True,many=False)
    title = serializers.CharField(default="lol",read_only=True)
    clients = UserSerializer(many=True, read_only=True)

    class Meta:
        model = models.MessageThread
        fields = ('uuid_id','title','clients','last_message','unread_count','timestamp','last_activity')


class MessageThreadListSerializer(serializers.ListSerializer):
    child = MessageThreadSerializer()
    many = True
    allow_null = True
