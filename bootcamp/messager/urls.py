from django.conf.urls import url

from bootcamp.messager import views

app_name = 'messager'
urlpatterns = [
    url(r'^load-inbox/$',views.LoadInbox.as_view()),
    url(r'^load-messages/$',views.LoadMessages.as_view()),
    url(r'^action/$',views.EditMembership.as_view()),
]
