from channels.generic.websocket import AsyncJsonWebsocketConsumer
from channels.db import database_sync_to_async

from django.contrib.auth import get_user_model

from bootcamp.messager.models import MessageThread, UnreadReceipt
from bootcamp.messager import serializers
from bootcamp.messager.fcm import send_event_via_fcm


class MessagerConsumer(AsyncJsonWebsocketConsumer):
    webrtc_partner_channel = None
    is_in_call = False

    async def connect(self):
        """Consumer Connect implementation, to validate user status and prevent
        non authenticated user to take advante from the connection."""
        if (not "user" in self.scope) or self.scope["user"].is_anonymous:
            # Reject the connection
            await self.close()

        else:
            # Accept the connection
            await self.channel_layer.group_add(f"{self.scope['user'].id}", self.channel_name)
            await self.update_user_status(self.scope['user'], True)
            await self.accept()

    async def disconnect(self, close_code):
        """Consumer implementation to leave behind the group at the moment the
        closes the connection."""
        if not self.scope["user"].is_anonymous:
            await self.channel_layer.group_discard(f"{self.scope['user'].id}", self.channel_name)
            await self.update_user_status(self.scope['user'], False)

    async def receive_json(self, content):
        """User sends a message

         - read all messages if data is read message
         - send message to thread and group socket if text message
         - Message is sent to the group associated with the message thread
        """
        if 'read' in content:
            # client specifies they have read a message that was sent
            thread = await database_sync_to_async(MessageThread.objects.get)(uuid_id=str(content['read']),clients=self.scope["user"])
            await database_sync_to_async(thread.mark_read)(self.scope["user"])
        elif 'message' in content:
            message = content['message']
            # extra security is added when we specify clients=p
            thread = await database_sync_to_async(MessageThread.objects.get)(uuid_id=message['id'],clients=self.scope["user"])
            #TODO Check Thread is null, if not handled CRASH expected.
            # forward chat message over group channel
            new_message = await database_sync_to_async(thread.add_message_text)(message['text'],self.scope["user"])
            new_message_serialized = serializers.MessageSerializer(new_message).data
            await self.channel_layer.group_send(
                    f"{self.scope['user'].id}", {
                        "type": "chat.message",
                        "message": new_message_serialized,
                    }
                )
            for c in await database_sync_to_async(thread.clients.exclude)(id=self.scope["user"].id):
                await database_sync_to_async(UnreadReceipt.objects.create)(recipient=c,thread=thread,message=new_message)
                if c.is_online:
                    await self.channel_layer.group_send(
                            f"{c.id}", {
                                "type": "chat.message",
                                "message": new_message_serialized,
                            }
                        )
                else:
                    #TODO: new_message.text should be replaced by get_text for messages with no texts
                    notification_serialized = {
                        "message_title":str(new_message.sender.displayName),
                        "message_body":new_message.text,
                        "thread_id":str(thread.uuid_id),
                        "peer_display_name":str(new_message.sender.displayName),
                        "peer_id":str(new_message.sender.id),
                        "peer_avatar":str(new_message.sender.avatar),
                    }
                    send_event_via_fcm(c, notification_serialized)

        elif 'type' in content:
            if content['type'] == "Offer":
                if not self.is_in_call:
                    self.webrtc_partner_channel = None
                    self.is_in_call = True
                    await self.channel_layer.group_send(
                            content['target'], {
                                "type": "webrtc.offer",
                                "message": content,
                                "channel": self.channel_name
                            }
                        )
            elif content['type'] == "Reply":
                if (not self.is_in_call) and (self.webrtc_partner_channel is not None):
                    self.is_in_call = True
                    await self.channel_layer.send(
                            self.webrtc_partner_channel, {
                                "type": "webrtc.reply",
                                "message": content,
                                "channel": self.channel_name
                            }
                        )
                    await self.channel_layer.group_send(
                            f"{self.scope['user'].id}", {
                                "type": "webrtc.reset",
                                "message": content,
                                "webrtc_partner_channel": self.webrtc_partner_channel
                            }
                        )
            elif content['type'] == "Ice":
                if self.is_in_call:
                    await self.channel_layer.group_send(
                            content['target'], {
                                "type": "webrtc.ice",
                                "message": content,
                                "channel": self.channel_name
                            }
                        )
            elif content['type'] == "Close":
                if self.webrtc_partner_channel is not None:
                    await self.channel_layer.send(
                            self.webrtc_partner_channel, {
                                "type": "webrtc.close",
                                "message": content,
                                "channel": self.channel_name
                            }
                        )
                    await self.channel_layer.group_send(
                            f"{self.scope['user'].id}", {
                                "type": "webrtc.reset",
                                "message": content,
                                "webrtc_partner_channel": self.webrtc_partner_channel
                            }
                        )
                self.webrtc_partner_channel = None
                self.is_in_call = False

    async def chat_message(self, event):
        """chat.message type"""
        message = event['message']
        await self.send_json(content=message)

    async def webrtc_offer(self, event):
        if not self.is_in_call:
            self.webrtc_partner_channel = event['channel']
            message = event['message']
            await self.send_json(content=message)

    async def webrtc_reply(self, event):
        if self.is_in_call and (self.webrtc_partner_channel is None):
            self.webrtc_partner_channel = event['channel']
            message = event['message']
            await self.send_json(content=message)

    async def webrtc_ice(self, event):
        if self.webrtc_partner_channel == event['channel']:
            message = event['message']
            await self.send_json(content=message)

    async def webrtc_close(self, event):
        if self.webrtc_partner_channel == event['channel']:
            self.webrtc_partner_channel = None
            if self.is_in_call:
                message = event['message']
                await self.send_json(content=message)
                self.is_in_call = False
        if self.is_in_call and (self.webrtc_partner_channel is None):
            message = event['message']
            await self.send_json(content=message)
            self.is_in_call = False

    async def webrtc_reset(self, event):
        if (not self.is_in_call) and self.webrtc_partner_channel == event['webrtc_partner_channel']:
            self.webrtc_partner_channel = None

    @database_sync_to_async
    def update_user_status(self, user, status):
        """
        Updates the user `status`.
        `status` is boolean
        """
        return get_user_model().objects.filter(id=user.id).update(is_online=status)
