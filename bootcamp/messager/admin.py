from django.contrib import admin
from bootcamp.messager.models import Message, MessageThread, UnreadReceipt


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ("sender", "thread", "timestamp")
    list_filter = ("sender", "thread")

@admin.register(MessageThread)
class MessageThreadAdmin(admin.ModelAdmin):
    list_display = ("title", "last_message")
    list_filter = ("title", "clients")

@admin.register(UnreadReceipt)
class UnreadReceiptAdmin(admin.ModelAdmin):
    list_display = ("message", "thread", "recipient", "timestamp")
    list_filter = ("message", "thread", "recipient")
