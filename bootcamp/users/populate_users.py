import os
import os.path
import itertools
from subprocess import call

from model_mommy import mommy
from model_mommy.recipe import seq

from bootcamp.users.models import User


heic_fn = '/home/ubuntu/public_html/getfern.app/assets/surfer_1440x960.heic'
User.objects.filter(username__startswith="testuser").delete()
users = mommy.make('users.User', first_name='Test', last_name='User', username=seq('testuser'), _quantity=1)
for user in users:
    user.set_password('secret')
    user.picture = f"profile_pics/pic0_{user.username}.heic"
    user.picture1 = f"profile_pics/pic1_{user.username}.heic"
    user.picture2 = f"profile_pics/pic2_{user.username}.heic"
    user.picture3 = f"profile_pics/pic3_{user.username}.heic"
    user.picture4 = f"profile_pics/pic4_{user.username}.heic"
    user.picture5 = f"profile_pics/pic5_{user.username}.heic"
    for i in range(6):
        out = f"/home/ubuntu/public_html/getfern.app/media/profile_pics/pic{i}_{user.username}.heic"
        call(['convert', heic_fn, '-gravity', 'Center', '-pointsize', '30', '-annotate', '0', f"' {user.username}{i} '", out])
    user.save()
