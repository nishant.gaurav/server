from django.conf.urls import url
from django.views.generic import TemplateView

from bootcamp.users import views

app_name = 'users'
urlpatterns = [
    url(
        regex=r'^$',
        view=views.UserListView.as_view(),
        name='list'
    ),
    url(
        regex=r'^~redirect/$',
        view=views.UserRedirectView.as_view(),
        name='redirect'
    ),
    url(
        regex=r'^~update/$',
        view=views.UserUpdate.as_view(),
        name='update'
    ),
    url(
        regex=r'^~validate/$',
        view=views.UserValidationView.as_view(),
        name='validate'
    ),
    url(
        regex=r'^(?P<username>[\w+]+)/$',
        view=views.UserDetailView.as_view(),
        name='detail'
    ),
    url(
        regex=r'^~upload/(?P<filename>[\w-]+)/$',
        view=views.ImageUploadView.as_view(),
        name='upload'
    ),
    url(
        regex=r'^~proof/(?P<filename>[\w-]+)/$',
        view=views.ProofUploadView.as_view(),
        name='proof'
    ),
    url(
        regex=r'^~remove/$',
        view=views.ImageDeleteView.as_view(),
        name='remove'
    ),
    url(
        regex=r'^~reorder/$',
        view=views.ImageReorderView.as_view(),
        name='reorder'
    ),
    url(
        regex=r'^~login/$',
        view=views.GoogleSignInView.as_view(),
        name='google_sign_in'
    ),
    url(
        regex=r'^~logout/$',
        view=views.FCMLogoutView.as_view(),
        name='fcm_logout'
    ),
    url(
        regex=r'^~logoutall/$',
        view=views.FCMLogoutAllView.as_view(),
        name='fcm_logoutall'
    ),
    url(
        regex=r'^~location/$',
        view=views.UpdateLocationView.as_view(),
        name='update_location'
    ),
    url(
        regex=r'^~fcmtoken/$',
        view=views.RegistrationToken.as_view(),
        name='token-detail'
    ),
]
