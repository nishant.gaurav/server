from rest_framework import serializers

from bootcamp.users.models import User, UserImage, SEX_CHOICES, Job, School, Location, Phone, UserRegistrationToken

from django.contrib.gis.geos import fromstr

class UserImageSerializer(serializers.ModelSerializer):
    url = serializers.CharField(source='image.url', read_only=True)
    class Meta:
        model = UserImage
        fields = ('url','uuid')

class JobSerializer(serializers.ModelSerializer):
    class Meta:
        model = Job
        fields = '__all__'

class SchoolSerializer(serializers.ModelSerializer):
    class Meta:
        model = School
        fields = '__all__'

class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = '__all__'

class PhoneSerializer(serializers.ModelSerializer):
    class Meta:
        model = Phone
        fields = '__all__'

user_common_fields = ('id','displayName','last_name','avatar','images','sex','birth_date','short_bio','bio','job','education','city','is_active','is_new','is_superuser','show_me')

class UserSerializer(serializers.ModelSerializer):
    sex = serializers.ChoiceField(choices=SEX_CHOICES, default='Male')
    images = UserImageSerializer(source='userimage_set', many=True, read_only=True)
    job = serializers.CharField(source='job_title.title', default="", read_only=True)
    education = serializers.CharField(source='school.name', default="", read_only=True)
    city = serializers.CharField(source='location.name', default="", read_only=True)
    invite_code = serializers.CharField(read_only=True)
    invite_count = serializers.IntegerField(read_only=True)
    likes_remaining = serializers.IntegerField(read_only=True)
    superlikes_remaining = serializers.IntegerField(read_only=True)
    user_count = serializers.SerializerMethodField()
    ig_id = serializers.CharField(read_only=True)
    ig_access_token = serializers.CharField(read_only=True)
    ig_username = serializers.CharField(read_only=True)
    ln_id = serializers.CharField(read_only=True)
    ln_access_token = serializers.CharField(read_only=True)
    ln_url = serializers.CharField(read_only=True)

    class Meta:
        model = User
        fields = (*user_common_fields,'ig_id','ig_access_token','ig_username','ln_id','ln_access_token','ln_url','invite_code','invite_count','superlikes_remaining','likes_remaining','user_count')

    def get_user_count(self, obj):
        return User.objects.count()

class UserListSerializer(serializers.ModelSerializer):
    sex = serializers.ChoiceField(choices=SEX_CHOICES, default='Male')
    images = UserImageSerializer(source='userimage_set', many=True, read_only=True)
    job = serializers.CharField(source='job_title.title', default="", read_only=True)
    education = serializers.CharField(source='school.name', default="", read_only=True)
    city = serializers.CharField(source='location.name', default="", read_only=True)
    distance = serializers.SerializerMethodField(read_only=True)
    smaller_radius = serializers.IntegerField(read_only=True)
    superlikemessage = serializers.CharField(read_only=True)
    ig_id = serializers.CharField(read_only=True)
    ig_access_token = serializers.CharField(read_only=True)
    ig_username = serializers.CharField(read_only=True)
    ln_id = serializers.CharField(read_only=True)
    ln_access_token = serializers.CharField(read_only=True)
    ln_url = serializers.CharField(read_only=True)

    class Meta:
        model = User
        fields = (*user_common_fields,'ig_id','ig_access_token','ig_username','ln_id','ln_access_token','ln_url','distance','smaller_radius','last_location','superlikemessage')

    def get_distance(self, obj):
        if hasattr(obj, 'distance'):
            return round(obj.distance.m, 1)
        else:
            return None

    def to_representation(self, instance):
        ret = super(UserListSerializer, self).to_representation(instance)
        pnt = fromstr(ret['last_location'])
        ret['last_location'] = {'longitude': pnt.coords[0], 'latitude': pnt.coords[1]}
        return ret

# MARK: FCM

class UserRegistrationTokenSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    registration_token = serializers.ReadOnlyField(source='registration_token')

    class Meta:
        model = UserRegistrationToken
        fields = ('user', 'registration_token')