import uuid
from datetime import timedelta

from django.utils import timezone
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.signals import user_logged_in, user_logged_out
from django.contrib.gis.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.contrib.gis.geos import Point
from django.contrib.postgres.fields import ArrayField
from django.dispatch import receiver

from bootcamp.notifications.models import Notification, notification_handler

SEX_CHOICES = (
    ('F', 'Female',),
    ('M', 'Male',),
)

def get_opposed_sex(sex):
    return 'M' if sex == 'F' else 'F'

class AccessToken(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)
    token = models.CharField(_("access_token"), blank=True, null=False, default="", max_length=1024)
    site = models.CharField(_("site"), blank=True, null=False, default="", max_length=16)
    timestamp = models.DateTimeField(blank=True, auto_now_add=True)

class UserImage(models.Model):
    uuid = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=False, on_delete=models.CASCADE)
    rank = models.IntegerField(blank=False, null=False, default=0)
    image = models.ImageField(upload_to='user_pics/')

    class Meta:
        ordering = ['rank']

class UserProof(models.Model):
    uuid = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)
    image = models.ImageField(upload_to='user_pics/')

@receiver(models.signals.post_delete, sender=UserImage)
def remove_file_from_s3(sender, instance, using, **kwargs):
    instance.image.delete(save=False)

class AppVersion(models.Model):
    code = models.IntegerField(unique=True, null=False, blank=False)
    expiry = models.DateTimeField(null=False)

    def __str__(self):
        return str(self.code)

class Job(models.Model):
    title = models.CharField(_('Job title'), max_length=50, null=False, blank=False)

    def __str__(self):
        return self.title

class School(models.Model):
    name = models.CharField(_('Univerisity Name'), max_length=100, null=False, blank=False)

    def __str__(self):
        return self.name

class Location(models.Model):
    name = models.CharField(_('City name'), max_length=100, null=False, blank=False)

    def __str__(self):
        return self.name

class Phone(models.Model):
    number = models.CharField(_(''), max_length=20, blank=False, null=False, default="", unique=True)
    country_prefix = models.CharField(_(''), max_length=5, blank=False, null=False, default="")
    national_number = models.CharField(_(''), max_length=15, blank=False, null=False, default="")
    is_whatsapp_verified = models.BooleanField(blank=True, default=False)
    accountkit_id = models.CharField(_(''), max_length=20, blank=False, null=False, default="")

def minimum_age():
    return timezone.now().date() - timedelta(days=6575)

def encodeN(n,N,D="0123456789abcdefghijklmnopqrstuvwxyz"):
    return (encodeN(n//N,N)+D[n%N]).lstrip("0") if n>0 else "0"

class User(AbstractUser):
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    displayName = models.CharField(_("User's first (display) name"), blank=True, null=False, default="", max_length=50)
    phone = models.OneToOneField(Phone, blank=False, null=True, on_delete=models.SET_NULL)
    is_online = models.BooleanField(blank=True, default=False)
    is_new = models.BooleanField(blank=True, default=False)
    birth_date = models.DateField(blank=False, null=False, default=minimum_age)
    sex = models.CharField(max_length=1, choices=SEX_CHOICES, db_index=True, blank=False, null=True)
    last_location = models.PointField(max_length=40, blank=True, null=True, default=Point(-157.8583,21.3069))
    picture = models.ForeignKey(UserImage, null=True, blank=True, on_delete=models.SET_NULL, related_name="dp_of")
    location = models.ForeignKey(Location, null=True, blank=True, on_delete=models.SET_NULL, related_name="resident")
    job_title = models.ForeignKey(Job, null=True, blank=True, on_delete=models.SET_NULL, related_name="doer")
    school = models.ForeignKey(School, null=True, blank=True, on_delete=models.SET_NULL, related_name="student")
    personal_url = models.URLField(
        _('Personal URL'), max_length=255, blank=True, null=True)
    short_bio = models.CharField(
        _('Short bio'), max_length=60, blank=True, null=False, default="")
    bio = models.CharField(
        _('Describe yourself'), max_length=500, blank=True, null=False, default="")
    ig_id = models.CharField(_("Instagram id"), blank=True, null=False, default="", max_length=100)
    ig_access_token = models.CharField(_("Instagram access_token"), blank=True, null=False, default="", max_length=1024)
    ig_username = models.CharField(_("Instagram username"), blank=True, null=False, default="", max_length=100)
    ln_id = models.CharField(_("Linkedin id"), blank=True, null=False, default="", max_length=100)
    ln_access_token = models.CharField(_("Linkedin access_token"), blank=True, null=False, default="", max_length=1024)
    ln_url = models.CharField(_("Linkedin url"), blank=True, null=False, default="", max_length=100)
    show_me = models.BooleanField(blank=True, default=True)
    invited_by = models.ForeignKey('self', null=True, blank=True, on_delete=models.SET_NULL)
    app_version = models.ForeignKey(AppVersion, null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.username

    class Meta(object):
        unique_together = ('email',)

    @property
    def avatar(self):
        if self.picture:
            return self.picture.image.url
        if self.userimage_set.first():
            return self.userimage_set.first().image.url
        return ""

    @property
    def invite_code(self):
        return self.username

    @property
    def invite_count(self):
        return User.objects.filter(invited_by=self).count()

    @property
    def likes_remaining(self):
        if (self.count_update_date != timezone.now().date()):
            self.likes_count = 0
            self.superlikes_count = 0
            self.count_update_date = timezone.now().date()
            self.save()
        return 5 - self.likes_count

    @property
    def superlikes_remaining(self):
        if (self.count_update_date != timezone.now().date()):
            self.likes_count = 0
            self.superlikes_count = 0
            self.count_update_date = timezone.now().date()
            self.save()
        return 1 - self.superlikes_count

    def add_like(self):
        if (self.count_update_date != timezone.now().date()):
            self.likes_count = 0
            self.superlikes_count = 0
            self.count_update_date = timezone.now().date()
            self.save()
        if (self.likes_count < 5):
            self.likes_count = self.likes_count + 1
            self.save()
            return True
        else:
            return False

    def add_superlike(self):
        if (self.count_update_date != timezone.now().date()):
            self.likes_count = 0
            self.superlikes_count = 0
            self.count_update_date = timezone.now().date()
            self.save()
        if (self.likes_count < 5 and self.superlikes_count < 1):
            self.likes_count = self.likes_count + 1
            self.superlikes_count = self.superlikes_count + 1
            self.save()
            return True
        else:
            return False

    @property
    def get_opposed_sex(self):
        return get_opposed_sex(self.sex)

    def get_absolute_url(self):
        return reverse('users:detail', kwargs={'username': self.username})

    def get_profile_name(self):
        if self.displayName:
            return self.displayName
        return self.username


def broadcast_login(sender, user, request, **kwargs):
    """Handler to be fired up upon user login signal to notify all users."""
    notification_handler(user, "global", Notification.LOGGED_IN)


def broadcast_logout(sender, user, request, **kwargs):
    """Handler to be fired up upon user logout signal to notify all users."""
    notification_handler(user, "global", Notification.LOGGED_OUT)


# user_logged_in.connect(broadcast_login)
# user_logged_out.connect(broadcast_logout)


# MARK: FCM

class UserRegistrationTokenManager(models.Manager):

    def create_and_set_user_token(self, user, token):
        object, created = UserRegistrationToken.objects.get_or_create(user=user)
        object.registration_token = token
        object.save()

        return object

    def delete_user_token(self, user):
        try:
            UserRegistrationToken.objects.get(user=user).delete()
        except UserRegistrationToken.DoesNotExist:
            pass

    def get_token_if_exists(self, user):
        try:
            return UserRegistrationToken.objects.get(user=user).registration_token
        except UserRegistrationToken.DoesNotExist:
            return None


class UserRegistrationToken(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        to_field='id',
        primary_key=True)
    registration_token = models.CharField(max_length=250, null=False, default="")
    created = models.DateTimeField(auto_now_add=True)

    objects = UserRegistrationTokenManager()
