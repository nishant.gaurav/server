import numpy as np
import random
import os
import os.path
import itertools
from subprocess import call

from django.core.management.base import BaseCommand
from django.contrib.gis.geos import Point

from bootcamp.users.models import User, UserImage, get_opposed_sex


class Command(BaseCommand):
    help = 'generate 1M users for Warsaw with random ' \
           'sex, age, orientation, radius, location'

    def add_arguments(self, parser):
        parser.add_argument('count', type=int)
        parser.add_argument('prefix', type=str)
        parser.add_argument('input_file', type=str)

    def handle(self, *args, **options):
        count = options['count']
        prefix = options['prefix']
        input_file = options['input_file']

        if User.objects.filter(username__startswith=f"{prefix}").exists():
            return

        kolkata__lat__min = 22.5
        kolkata__lat__max = 22.6
        kolkata__lng__min = 88.3
        kolkata__lng__max = 88.45

        for i in range(count):
            new_random_lat = random.uniform(kolkata__lat__min, kolkata__lat__max)
            new_random_lng = random.uniform(kolkata__lng__min, kolkata__lng__max)
            user_age = random.randrange(18, 55)
            user_delta_plus = random.choice([1, 2, 3, 5, 8, 13])
            user_delta_minus = random.choice([1, 2, 3, 5, 8, 13])
            user_sex = random.choice(['F', 'M'])
            sexual_orientation = ['hetero', 'homo']
            chosen_at_random_sexual_orientation = np.random.choice(
                sexual_orientation, p=[0.95, 0.05])

            if chosen_at_random_sexual_orientation == 'homo':
                user_preferred_sex = user_sex
            else:
                user_preferred_sex = get_opposed_sex(user_sex)

            u = User.objects.create(
                username=f"{prefix}{i}",
                displayName=f"{prefix}{i}",
                age=user_age,
                sex=user_sex,
                preferred_sex=user_preferred_sex,
                preferred_age_min=(user_age-user_delta_minus),
                preferred_age_max=(user_age+user_delta_plus),
                last_location=Point(float(new_random_lng),
                                    float(new_random_lat)),
                preferred_radius=random.choice([5, 10, 15, 20, 25, 30])
            )

            for j in range(5):
                out = f"/home/ubuntu/public_html/getfern.app/media/profile_pics/{prefix}{i}_pic{j}.jpg"
                call(['convert', input_file, '-gravity', 'Center', '-pointsize', '30', '-annotate', '0', f"' {prefix}{i}_pic{j} '", out])
                img = UserImage.objects.create(
                    user = u,
                    image = f"profile_pics/{prefix}{i}_pic{j}.jpg"
                )
                if (i+j)%5 == 0:
                    u.picture = img
                    u.save()
